#! /usr/bin/python3
################################################################################
# Copyright 2023 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

from put_basemaps import put_basemaps
from put_dem_nwp import put_dem_nwp
from put_maps_interpol import put_maps_interpol
from put_nowcasts import put_nowcasts
from put_pois import put_pois
from put_profile_forecasts import put_profile_forecasts
from put_shapes import put_shapes
from put_solar import put_solar
from put_vstations import put_vstations

import argparse
import folium
from folium.plugins import MarkerCluster
import glob
import os

_domains_path = "~opera/domains"
_domains_path = os.path.expanduser(_domains_path)
_map_init_location = [47, 11.5]

def _get_awsome_paths():
    opera_paths = { # TODO: get from configs what's possible
            "map_path": "/var/www/html/index.html",
            "microregions": os.path.expanduser("~data/static_data/shapefiles/eaws-micro-regions/"),
            "observed_profiles": os.path.expanduser("~snowpack/profile-forecasts/output/observed_profiles.json"),
            "dem_from_nwp": os.path.expanduser("~snowpack/profile-forecasts/data/ecmwf-inca/nwp_stacked.nc"),
            "vstations": os.path.expanduser("~snowpack/forecasts/input/vstations-tirol.csv"),
            "nowcast": os.path.expanduser("~snowpack/opera/output"),
            "wrf": os.path.expanduser("~wrf/wrf/output"),
            "solar": os.path.expanduser("~snowpack/solar")
    }
    return opera_paths

def _init_map(minimal: bool=False):
    _zoom_start = 9
    fmap = folium.Map(location=_map_init_location, zoom_start=_zoom_start, tiles=None)
    fmap = put_basemaps(fmap)    
    return fmap

def _finalize_map(fmap: folium.Map, output_path: str, minimal: bool=False) -> folium.Map:
    fmap = _add_plugins(fmap, minimal)
    fmap.save(output_path)
    print(f"[i] Map generated in {output_path}")

def _add_plugins(fmap: folium.Map, minimal: bool=False) -> folium.Map:
    if not minimal:
        folium.plugins.Geocoder( # search bar (cities, ...)
            position="bottomright"
        ).add_to(fmap)
        folium.plugins.MiniMap(
            toggle_display=True,
            position="bottomright",
            tile_layer="OpenStreetMap"
        ).add_to(fmap)
        folium.plugins.Fullscreen(
            position="topright",
            title="Full screen",
            title_cancel="Exit full screen",
            force_separate_button=True
        ).add_to(fmap)
        folium.plugins.LocateControl(
            position="bottomright"
        ).add_to(fmap)
    folium.plugins.MousePosition(
        position="bottomright"
    ).add_to(fmap)
    folium.LayerControl(
        position="topleft",
    ).add_to(fmap)
    #folium.plugins.MeasureControl(
    #    position="topleft"
    #).add_to(fmap)
    return fmap

def full_run():

    from put_wrf import put_wrf # this package should not be required for the standalone run

    print("[i] Initializing map...")
    fmap = _init_map()
    print("[i] Gathering data...")
    domains = glob.glob(f"{_domains_path}/*.xml")
    domains.sort()
    domains = [os.path.basename(dom) for dom in domains]
    domains = [dom[:-4] for dom in domains]

    opaths = _get_awsome_paths()
    used_shape_paths = []
    dom_separator = "⎯" * 10
    for dom in domains:
        dom_name = f"{dom_separator} {dom.upper()} {dom_separator}"
        print(f"[i] Working on domain {dom}...")
        profiles_marker_cluster = MarkerCluster(name=dom_name, show=False).add_to(fmap)
        op = opaths["microregions"]
        # if domains use the same shapefiles put them only once
        # (rethink if we have a TreeView):
        if not op in used_shape_paths:
            fmap = put_shapes(fmap, dom, op)
            used_shape_paths.append(op)
        fmap = put_vstations(fmap, dom, opaths["vstations"])
        fmap = put_profile_forecasts(fmap, dom, opaths["observed_profiles"])
        fmap = put_nowcasts(fmap, dom, opaths["nowcast"])
        fmap = put_pois(fmap, dom)
        fmap = put_wrf(fmap, dom, opaths["wrf"])
        fmap = put_maps_interpol(fmap, dom)
        fmap = put_dem_nwp(fmap, dom, opaths["dem_from_nwp"])
        #fmap = put_solar(fmap, dom, opaths["solar"])
    _finalize_map(fmap, opaths["map_path"])

def standalone_run(args):
    _standalone_domain = "profile-forecasts"
    print("[i] Initializing map...")
    fmap = _init_map(minimal=True)
    fmap = put_shapes(fmap, _standalone_domain, args["microregions"])
    if args["vstations"]:
        fmap = put_vstations(fmap, _standalone_domain, args["vstations"])
    if args["observed_profiles"]:
        fmap = put_profile_forecasts(fmap, _standalone_domain, args["observed_profiles"])
    if args["dem_from_nwp"]:
        fmap = put_dem_nwp(fmap, _standalone_domain, args["dem_from_nwp"])
    _finalize_map(fmap, args["map_path"], minimal=True)

if __name__ == "__main__":
    """
    Here we check if data paths are given as command line args.
    If so, we use that and gererate only the one requested map.
    If not, we assume operational use within the full AWSOME toolchain
    and try to find whichever data is available in its folder structure.
    """
    parser = argparse.ArgumentParser()
    available_args = {
        "--map_path": False,
        "--microregions": False,
        "--vstations": False,
        "--observed_profiles": False,
        "--dem_from_nwp": False
    }
    for arg in available_args:
        parser.add_argument(arg, required=available_args[arg])
    args = vars(parser.parse_args())

    if args["map_path"] == None: # operational use within AWSOME
        if not os.path.exists(_domains_path):
            print("[E] No map path available - call with --map_path <your_path>. Does not looke like an AWSOME machine either - quitting.")
            sys.exit()
        full_run()
    else: # standalone operational use
        standalone_run(args)
