################################################################################
# Copyright 2023 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

import datetime
import folium

def put_basemaps(fmap: folium.Map, minimal: bool=False) -> folium.Map:
    """Map for visualizing and interacting with AWSOME data"""

    # some providers: https://leaflet-extras.github.io/leaflet-providers/preview/
    map_collection = []
    _cc = {}

    # Maps over Scandinavia. Also, example of using multiple map sources in single layer
    #
    #maps_kartverket = {
    #    "Norwegian Topo 4": [
    #        "https://api.lantmateriet.se/open/topowebb-ccby/v1/wmts/token/9a73d194-b3c4-399e-864b-52f568a87631/?layer=topowebb_nedtonad&style=default&tilematrixset=3857&Service=WMTS&Request=GetTile&Version=1.0.0&Format=image/png&TileMatrix={z}&TileCol={x}&TileRow={y}",
    #        "https://opencache.statkart.no/gatekeeper/gk/gk.open_gmaps?layers=topo4graatone&zoom={z}&x={x}&y={y}",
    #    ]
    #}
    #_cc["kartverket"] = [
    #    'Map data: &copy; <a href="https://www.lantmateriet.se/sv/geodata/vara-produkter/produktlista/topografisk-webbkarta-visning-oversiktlig/">Lantmäteriet</a> (<a href="https://creativecommons.org/publicdomain/zero/1.0/legalcode.sv">CC0</a>)',
    #    '&copy; <a href="https://www.kartverket.no/api-og-data/vilkar-for-bruk">Kartverket</a> (<a href="https://creativecommons.org/licenses/by/4.0/deed.no">CC BY 4.0</a>)',
    #]
    #map_collection.append((maps_kartverket, _cc["kartverket"], [{'max_native_zoom': 14, 'min_zoom': 5},{'max_zoom': 20, 'min_zoom': 5}]))

    maps_albina = {
        "Albina": "https://static.avalanche.report/tms/{z}/{x}/{y}.png"
    }
    _cc["albina"] = '&copy; <a href="https://avalanche.report">avalanche.report</a>'

    if minimal:
        folium.TileLayer(tiles=maps_albina["Albina"], name="Albina", attr=_cc["albina"]).add_to(fmap)
        return fmap

    map_collection.append((maps_albina, _cc["albina"], {}))

    maps_basemap = {
            "Basemap Austria Surface": "https://mapsneu.wien.gv.at/basemap/bmapoberflaeche/grau/google3857/{z}/{y}/{x}.jpeg",
            "Basemap Austria Terrain": "https://mapsneu.wien.gv.at/basemap/bmapgelaende/grau/google3857/{z}/{y}/{x}.jpeg",
            "Basemap Austria Orthophoto": "https://mapsneu.wien.gv.at/basemap/bmaporthofoto30cm/normal/google3857/{z}/{y}/{x}.jpeg",
            "Basemap Austria Overlay": "https://mapsneu.wien.gv.at/basemap/bmapoverlay/normal/google3857/{z}/{y}/{x}.png"
    }
    _cc["basemap"] = 'Datenquelle: <a href="https://www.basemap.at">basemap.at</a>' 
    map_collection.append((maps_basemap, _cc["basemap"], {}))

    maps_otm = {
        "Open Topo Map": "https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png",
    }
    _cc["otm"] = 'Map data: &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)'
    map_collection.append((maps_otm, _cc["otm"], {}))

    _fetchdate = str(datetime.date.today() - datetime.timedelta(days=1)) # make sure there's an image available
    maps_nasa = {
        "NASA MODIS Terra (yesterday)": "https://map1.vis.earthdata.nasa.gov/wmts-webmerc/MODIS_Terra_CorrectedReflectance_TrueColor/default/" + _fetchdate + "/GoogleMapsCompatible_Level9/{z}/{y}/{x}.jpg"
        }
    _cc["nasa"] = 'Imagery provided by services from the Global Imagery Browse Services (GIBS), operated by the NASA/GSFC/Earth Science Data and Information System (<a href="https://earthdata.nasa.gov">ESDIS</a>) with funding provided by NASA/HQ.'
    map_collection.append((maps_nasa, _cc["nasa"], {}))

    maps_esri = {
        "Nat Geo World Map": "http://services.arcgisonline.com/arcgis/rest/services/NatGeo_World_Map/MapServer/tile/{z}/{y}/{x}",
        "ESRI Topographic": "http://services.arcgisonline.com/arcgis/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}",
        "ESRI Terrain Base": "http://services.arcgisonline.com/arcgis/rest/services/World_Terrain_Base/MapServer/tile/{z}/{y}/{x}",
        "ESRI Shaded Relief": "http://services.arcgisonline.com/arcgis/rest/services/World_Shaded_Relief/MapServer/tile/{z}/{y}/{x}",
        "ESRI World Imagery": "http://services.arcgisonline.com/arcgis/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}",
        "ESRI Ocean Base": "http://services.arcgisonline.com/arcgis/rest/services/Ocean/World_Ocean_Base/MapServer/tile/{z}/{y}/{x}",
        "ESRI Street Map": "http://services.arcgisonline.com/arcgis/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}"
    }
    _cc["esri"] = 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ, TomTom, Intermap, iPC, USGS, FAO, NPS, NRCAN, GeoBase, Kadaster NL, Ordnance Survey, Esri Japan, METI, Esri China (Hong Kong), and the GIS User Community'
    map_collection.append((maps_esri, _cc["esri"], {}))

    maps_builtin = {
        "OpenStreetMap": "openstreetmap",
        "CartoDB Dark": "Cartodb dark_matter"
    }
    map_collection.append((maps_builtin, None, {}))

    enabled_maps = ["Albina", "Basemap Austria Terrain", "Open Topo Map", "NASA MODIS Terra (yesterday)", "OpenStreetMap"]
    first = True
    for map_dict, attribution, layer_params in map_collection: # tuples with (map dict, attribution, layer_params)
        for layer_name, url in map_dict.items(): # iterate through each map set
            if not layer_name in enabled_maps:
                continue
            if not attribution: # no description means built-in map
                folium.TileLayer(
                    url,
                    name=layer_name,
                    show=first,
                    **layer_params
                ).add_to(fmap)
            elif isinstance(url, list): # if all tuple members are lists, there are multiple map sources for layer
                layer_group = folium.FeatureGroup(
                    name=layer_name,
                    overlay=False,
                    show=first
                )
                layer_group.add_to(fmap)
                for url, attribution, layer_params in zip(url, attribution, layer_params):
                    folium.TileLayer(
                        tiles=url,
                        name=layer_name,
                        show=first,
                        attr=attribution,
                        **layer_params
                    ).add_to(layer_group)
            else:
                folium.TileLayer(
                    tiles=url,
                    name=layer_name,
                    show=first,
                    attr=attribution,
                    **layer_params
                ).add_to(fmap)
            first = False
    return fmap
