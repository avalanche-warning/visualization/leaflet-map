################################################################################
# Copyright 2023 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

import awset
import branca.colormap
from branca.element import MacroElement
import cartopy.crs as ccrs
import folium
from jinja2 import Template
import matplotlib.pyplot as plt
import numpy as np
import os
import xarray as xr

class BindColormap(branca.element.MacroElement):
    """Binds a colormap to a given layer."""
    def __init__(self, layer: folium.raster_layers.ImageOverlay,
            colormap: branca.colormap.StepColormap, show: bool=True):
        super(BindColormap, self).__init__()
        self.layer = layer
        self.colormap = colormap
        self._init_show = u"block" if show else u"none"

        # https://stackoverflow.com/questions/56164535/adding-colormap-legend-to-folium-map
        _template = u"""
        {% macro script(this, kwargs) %}
            {{this.colormap.get_name()}}.svg[0][0].style.display = '"""
        _template = _template + self._init_show
        _template = _template + u"""';
            {{this._parent.get_name()}}.on('overlayadd', function (eventLayer) {
                if (eventLayer.layer == {{this.layer.get_name()}}) {
                    {{this.colormap.get_name()}}.svg[0][0].style.display = 'block';
                }});
            {{this._parent.get_name()}}.on('overlayremove', function (eventLayer) {
                if (eventLayer.layer == {{this.layer.get_name()}}) {
                    {{this.colormap.get_name()}}.svg[0][0].style.display = 'none';
                }});
        {% endmacro %}
        """
        self._template = branca.element.Template(_template)

def put_dem_nwp(fmap: folium.Map, domain: str, dem_path: str, show=False) -> folium.Map:
    """Visualize DEM (from NWP data)"""
    if domain == "profile-forecasts": # standalone run
        enabled = True
    else:
        enabled = awset.is_enabled("forecasts", domain)
    if not enabled:
        return fmap
    if not os.path.exists(dem_path):
        print("[E] No NWP DEM available.")
        return fmap

    opacity=0.65
    with xr.open_dataset(dem_path) as ds:
        easting  = ds.x.values
        northing = ds.y.values
        lat = ds.lat.values
        lon = ds.lon.values
        levels = np.arange(0,4000,400)

        # - Create a colormap for the contourf plot (optional) - #
        cmap = plt.get_cmap('terrain',levels[-1])  # Replace 'terrain' with any other colormap you prefer
        index_list = levels.astype(int).tolist()
        cmap_list  = cmap(index_list).tolist()
        cmap_folium = branca.colormap.LinearColormap(
                cmap_list, index=index_list, vmin=levels[0], vmax=levels[-1], caption="Elevation / m").to_step(len(levels)-1)

        # - Plot the raster layer using rasterio.plot.show() or matplotlib - #
        # extent  = [lon.min(), lat.min(), lon.max(), lat.max()]
        # extent2 = [easting.min(), northing.min(), easting.max(), northing.max()]
        fig, ax = plt.subplots(1,1,figsize=(8,8), subplot_kw={'projection':ccrs.Mercator()}) # PlatteCarre is the same
        #ax.contourf(lon, lat, ds.HGT_surface[0, :, :].values, cmap=cmap, levels=levels)
        ax.pcolormesh(lon, lat, ds.HGT_surface[0, :, :].values, cmap=cmap, shading='nearest')
        # rioplot.show(ds.HGT_surface[0, :, :].values, cmap=cmap,ax=ax) # extend = extend2
        ax.axis('off')  # Turn off axis for a cleaner plot
        os.makedirs('./tmp_data', exist_ok=True)
        tmp_file = './tmp_data/tmp_plot.png'
        fig.savefig(tmp_file, bbox_inches='tight', pad_inches=0)
        plt.close()

    """Add the raster layer to the Folium map"""
    # offset = [-0.005,-0.007,0.005,0.007] # [latmin,lonmin,latmax,lonmax], half pixels
    offset = [-0.005,+0.007,-0.005,+0.007] # [latmin,lonmin,latmax,lonmax], tuned (maybe more effective to get lon/lat of corresponding pixels?)
    ## Ultimate goal should be to visualize folium in MGI / Austria lambert projection
    rl_folium = folium.raster_layers.ImageOverlay(
        image=tmp_file,
        # bounds=[[lat.min(), lon.min()], [lat.max(), lon.max()]],
        bounds=[[lat.min()+offset[0], lon.min()+offset[1]], [lat.max()+offset[2], lon.max()+offset[3]]],  # consider half pixels at lower and upper boundaries!
        # bounds=[[float(northing.min()), float(easting.min())], [float(northing.max()), float(easting.max())]],
        origin='lower',
        opacity=opacity,
        pixelated=True,
        # transform=data_crs,
        name=f"{domain.capitalize()}: DEM from NWP for gridded simulations",
        # mercator_project=True,
        # colormap=lambda value: (0, 0, 0, 0),  # Set colormap to transparent (black) to allow the image to show through
        show=show
    ).add_to(fmap)

    """Adding colormap, layerControl, removing temporary file, ..."""
    fmap.add_child(cmap_folium)
    fmap.add_child(BindColormap(rl_folium, cmap_folium, show=show))
    os.remove(tmp_file)
    return fmap

