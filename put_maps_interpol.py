################################################################################
# Copyright 2023 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

import awset
import folium

def put_maps_interpol(fmap: folium.Map, domain: str):
    """Integrate our own WMS.

    MapServer always uses EPSG 3857 for tiled mode.
    """
    enabled = awset.is_enabled("maps", domain)
    if not enabled:
        return fmap

    #maps = { "geo": ["dem"], "grid": ["hs", "ta"] }
    maps = { "grid": ["hs"] }
    maps_interpol = {}

    for grid in maps:
        varlist = maps[grid]
        for var in varlist:
            key = f"{domain.capitalize()}: {var.upper()} [AWS interpolated]"
            maplink = "https://wms.alpine-software.at/?mode=tile&tile={x}+{y}+{z}"
            maplink += f"&layers={var}&map={grid}_{domain}"
            maps_interpol[key] = maplink

    for mapi in maps_interpol:
        folium.TileLayer(
            tiles=maps_interpol[mapi],
            name=mapi,
            opacity=0.8,
            overlay=True,
            show=False,
            attr='&copy; <a href="https://gitlab.com/avalanche-warning">awsome</a>'
        ).add_to(fmap)
    return fmap
