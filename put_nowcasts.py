#! /usr/bin/python3
################################################################################
# Copyright 2023 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

from awio.filetools import get_scriptpath
import awset
import awsmet
import branca
import base64
import folium
import glob
import json
import urllib.parse
import os

_server_profile_path = "/profiles"

def _get_pro_header(profile): # TODO: unify with smet parser
    header = {}
    with open(profile, "r") as fpro:
        for line in _generate_header(fpro):
            line = line.partition("#")[0]
            if not line:
                continue
            fields = [field.strip() for field in line.split("=")]
            header[fields[0]] = fields[1]
    return header

def _generate_header(profile):
    """Generator to yield all header fields of a PRO file."""
    next(profile) # skip first line
    for line in profile:
        line = line.strip()
        if line == "[HEADER]":
            break
        yield line

def put_nowcasts(fmap: folium.Map, domain: str, nowcast_path: str):
    enabled = awset.is_enabled("snowpack", domain)
    if not enabled:
        return fmap

    pro_path = os.path.join(nowcast_path, domain)
    if not os.path.exists(pro_path):
        print(f'[W] No nowcasts for domain "{domain}"')
        return fmap
    pro_files = os.listdir(pro_path)

    locations = []
    for pro in pro_files:
        if not pro.endswith(".pro"):
            continue
        profile = os.path.join(pro_path, pro)
        pro_header = _get_pro_header(profile)

        location_info = {
            "locationName": pro_header["StationName"],
            "stationId": pro[:-4],
            "latitude": pro_header["Latitude"],
            "longitude": pro_header["Longitude"],
            "elevation": pro_header["Altitude"],
            "angle": pro_header["SlopeAngle"],
            "aspect": pro_header["SlopeAzi"],
            "$internalURL": profile,
            "type": "snow"
        }
        locations.append(location_info)

    # Find wind stations. Throughout the map generation sometimes we
    # work with jsons, and sometimes we glob directories. Maybe everything
    # should be put in jsons at the point of running snowpack, on the other
    # hand this way we process everything that has been put online
    # (by whichever toolchain).
    _server_smet_path = f"/var/www/html{_server_profile_path}"
    wind_stations = os.path.join(_server_smet_path, "*_wind.smet")
    wind_stations = glob.glob(wind_stations)
    for wind_stat in wind_stations:
        smet_header = awsmet.SMETParser.get_header(wind_stat)
        location_info = {
            "locationName": smet_header["station_name"],
            "stationId": smet_header["station_id"],
            "latitude": smet_header["latitude"],
            "longitude": smet_header["longitude"],
            "elevation": smet_header["altitude"],
            "$internalURL": wind_stat,
            "type": "wind"
        }
        locations.append(location_info)

    os.makedirs("./tmp_data", exist_ok=True)
    temp_file_name = os.path.join(get_scriptpath(__file__), f"tmp_data/{domain}_nowcast_profile_locations.json")
    with open(temp_file_name, "w") as ofile:
        json.dump(locations, ofile)
    _add_map_cluster(fmap, domain, temp_file_name)
    return fmap

def _pick_color(station, faulty_stations):
    icon_color = "green"
    symbol_color = "white"
    error_msg = None
    for faulty in faulty_stations:
        if station["stationId"].startswith(faulty["stationId"]):
            error_msg = faulty["error"]
            if faulty["status"] == 1: # hard exit
                icon_color = "red"
            elif faulty["status"] == 2: # no valid data
                icon_color = "orange"
            elif faulty["status"] == 3: # used fallback ini
                icon_color = "green"
                symbol_color = "orange"
                error_msg = None # a slightly different color is enough - this happens often
            else:
                print(f'[w] Status code {faulty["status"]} not known for failed station.')
            break
    return error_msg, icon_color, symbol_color
        
def _add_map_cluster(fmap: folium.Map, domain: str, point_data_path: str):
    faulty_path = os.path.expanduser(f"~snowpack/opera/output/{domain}/faulty_stations.json")
    faulty_stations = {}
    if os.path.exists(faulty_path):
        with open(faulty_path, "r") as faulty:
            faulty_stations = json.load(faulty)

    nowcast_marker_cluster = folium.plugins.MarkerCluster(
        name=f"{domain.capitalize()}: SNOWPACK Nowcast").add_to(fmap)
    with open(point_data_path, "r") as infile:
        json_file = json.load(infile)

    for station in json_file:

        is_wind_stat = station["type"] == "wind"
        error_msg, icon_color, symbol_color = _pick_color(station, faulty_stations)
        url = station["$internalURL"]
        html = _get_popup_content(station, error_msg, icon_color, is_wind_stat)
        # iPhone 13 Mini: max size is ca. 650x500 when loading in portrait, bigger will not show
        iframe = branca.element.IFrame(html=html, width=650, height=500)
        popup = folium.Popup(iframe)

        # construct a marker item showing the slope exposition:
        if is_wind_stat:
            marker_icon = folium.Icon(prefix="fa", icon="wind", color="green", icon_color=symbol_color)
        else:
            if float(station["angle"]) == 0:
                marker_icon = folium.Icon(prefix="fa", icon="minus", color=icon_color,
                    icon_color=symbol_color) # flat field
            else:
                icon_kw = {"prefix": "fa", "color": icon_color, "icon": "arrow-up",
                    "icon_color": symbol_color}
                icon_angle = int(float(station["aspect"]))
                marker_icon = folium.Icon(angle=icon_angle, **icon_kw)

        folium.Marker(
            location = [station["latitude"], station["longitude"]],
            popup=popup,
            lazy=True,
            icon=marker_icon,
            tooltip=station["locationName"],
            draggable=False
        ).add_to(nowcast_marker_cluster)

def _get_popup_content(point_data: dict, error_msg: str=None, icon_color=None,
        is_wind_stat=False):
    """Create HTML page to use as popup for observed profiles."""

    if is_wind_stat:
        with open(os.path.join(get_scriptpath(__file__), "templates/popups/wind_station.html")) as popup_template:
            html = popup_template.read()
    else:
        with open(os.path.join(get_scriptpath(__file__), "templates/popups/nowcasts.html")) as popup_template:
            html = popup_template.read()

    if error_msg:
        html = html.replace("$ERROR_VISIBLE", "") # show the error <div>
        html = html.replace("$ERRORTEXT", error_msg.replace("\n", "<br>"))
        if icon_color == "orange": # TODO: get this from the color code automatically
            html_color = "#FFD6A1"
        elif icon_color == "red":
            html_color = "#FFB5A6"
        else:
            html_color = "#FFFFFF"
        html = html.replace("$ERRORCOLOR", html_color)
        html = html.replace("$NIVIZHEIGHT", "90%")
    else:
        html = html.replace("$ERROR_VISIBLE", "none") # hide the error <div>
        html = html.replace("$NIVIZHEIGHT", "100%")

    # we know the location of the pro file from the station id:
    hostname = awset.get(["host", "domain"], "awsome", "~opera/project") # domain of awsome server
    pro_link = f"{_server_profile_path}/{point_data['stationId']}.pro"
    pro_link = urllib.parse.quote_plus(pro_link)
    pro_link = f"https://models.{hostname}/niViz/?file=" + pro_link
    html = html.replace("$PRO_LINK", pro_link)

    if is_wind_stat:
        wind_link = f"{_server_profile_path}/{point_data['stationId']}_wind.smet"
        wind_link = urllib.parse.quote_plus(wind_link)
        wind_link = f"https://models.{hostname}/niViz/?file=" + wind_link
        html = html.replace("$WIND_LINK", wind_link)

    # The smet we must find from the station id on the server
    # (e. g. "ISEE1.smet" belongs to aspect "ISEE12.pro"):
    _server_smet_path = f"/var/www/html{_server_profile_path}/*.smet"
    smets = glob.glob(_server_smet_path)
    smet_link = None
    smet_link_forcing = None
    for smet in smets:
        statid = os.path.basename(smet)[:-5]
        if "_wind" in smet:
            continue # drawn separately
        if "_forcing" in smet: # we show the flat field forcing meteo data for each aspect
            statid = os.path.basename(smet.split("_")[0])
            if point_data["stationId"].startswith(statid):
                smet_link_forcing = urllib.parse.quote_plus(os.path.join("/profiles", os.path.basename(smet)))
                smet_link_forcing = f"https://models.{hostname}/niViz/?file=" + smet_link_forcing
        if point_data["stationId"].startswith(statid): # show meteo data for all aspects
            smet_link = f"{_server_profile_path}/{statid}.smet"
            smet_link = urllib.parse.quote_plus(smet_link)
            smet_link = f"https://models.{hostname}/niViz/?file=" + smet_link

    if smet_link:
        html = html.replace("$SMET_LINK", smet_link)
    else:
        #TODO: display n/a here instead (but should not happen):
        html = html.replace("$SMET_LINK", pro_link)
    if smet_link_forcing:
        html = html.replace("$SMET_FORCING_LINK", smet_link_forcing)
    else:
        html = html.replace("$SMET_FORCING_LINK", pro_link)

    return html
