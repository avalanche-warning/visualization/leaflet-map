################################################################################
# Copyright 2023 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

import os
import awset
import folium
from folium.plugins import MarkerCluster

def _get_pois(domain: str):
    pois = awset.nodelist_dict(["report", "pois"], domain=domain)
    return pois["poi"] # list with each individual poi

def put_pois(fmap: folium.Map, domain: str) -> folium.Map:

    if not awset.exists(["report", "pois"], domain=domain):
       return fmap

    hostname = awset.get(["host", "domain"], "awsome", "~opera/project")
    pois = _get_pois(domain)
    poi_marker_cluster = MarkerCluster(name=f"{domain.capitalize()}: POIs").add_to(fmap)
    with open(os.path.expanduser("~opera/dashboard/templates/poi.html", "r")) as popup_file:

        html_template = popup_file.read()

    for poi in pois:
        report_link = f"https://winter.{hostname}/?domain={domain}&poi={poi['locationName']}"
        html = html_template.replace("$POI_LINK", report_link)
        iframe = folium.IFrame(html, width=650, height=430)
        popup  = folium.Popup(iframe)
        tooltip = f'<b>{poi["locationName"]}</b>'
        icon_kw = {"prefix": "fa", "color": "cadetblue", "icon": "temperature-low"}
        marker_icon = folium.Icon(**icon_kw)

        latlon = poi["location"].split(",")
        latlon = (float(latlon[0]), float(latlon[1]))
        folium.Marker(
            location=[latlon[0], latlon[1]],
            popup=popup,
            lazy=True,
            icon=marker_icon,
            tooltip=tooltip,
            draggable=False
        ).add_to(poi_marker_cluster)
    return fmap

