################################################################################
# Copyright 2023 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

import awgeo
import awset
import base64
import json
import folium
from folium.plugins import MarkerCluster
import os

def put_profile_forecasts(fmap: folium.Map, domain: str, profiles_json_path: str) -> folium.Map:
    """Adding observed and simulated snow profile locations"""
    if domain == "profile-forecasts": # standalone run
        enabled = True
    else:
        enabled = awset.is_enabled("profile_forecasts", domain)
    if not enabled:
        return fmap

    if not os.path.exists(profiles_json_path):
        print('[W] No observed profiles found for domain "{domain}".')
        return fmap
    profiles_marker_cluster = MarkerCluster(name=f"{domain.capitalize()}: Profile Forecasts").add_to(fmap)
    with open(profiles_json_path, "r") as f:
        json_file = json.load(f)

    html = """
           <!DOCTYPE html>
           <html>
             <body style="margin: 0;">
               <img src="{}" alt="Snow stratigraphy profile" height=425 width=645>
             </body>
           </html>
        """.format

    for entry in json_file:
        url = entry["$externalURL"]
        iframe = folium.IFrame(html(url), width=650, height=430)
        popup  = folium.Popup(iframe)
        tooltip = f'<b>{entry["locationName"]}</b>'
        # TODO: transport degrees and slope angle to here
        aspect = awgeo.get_aspect(entry["aspect"])
        # construct a marker item showing the slope exposition:
        icon_kw = {"prefix": "fa", "color": "purple", "icon": "arrow-up"}
        marker_icon = folium.Icon(angle=aspect, **icon_kw)

        folium.Marker(
            location=[entry["latitude"], entry["longitude"]],
            popup=popup,
            lazy=True,
            icon=marker_icon,
            tooltip=tooltip,
            draggable=False
        ).add_to(profiles_marker_cluster)
    return fmap

