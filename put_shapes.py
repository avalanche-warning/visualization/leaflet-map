################################################################################
# Copyright 2023 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

import folium
import json
import os
from pathlib import Path

def region_style_function(feature):
    return {
        "fillColor": "grey", # polygon fill color
        "color": "#59C5FF", # line color
        "weight": 2, # line weight
        "fillOpacity": 0 # opacity of polygons
    }

def put_shapes(fmap: folium.Map, domain: str, shape_path: str) -> folium.Map:
    """Import shapefile polygons."""
    if not shape_path:
        print("[E] No path for region shapes. In standalone mode specify with --microregions <your_path>.")
        sys.exit()

    shapes_cluster = folium.plugins.MarkerCluster(name=f'{domain.capitalize()}: Shapefiles').add_to(fmap)
    for entry in os.scandir(shape_path):
        if ( entry.path.endswith(".json") and entry.is_file() and
                ("AT-07" in entry.path or "IT-32-BZ" in entry.path) ):
            # EAWS micro regions:
            with open(entry.path, "r") as f:
                json_file = json.load(f)
            sname = Path(entry).stem
            folium.GeoJson(json_file, style_function=region_style_function,
                name=f"{domain.capitalize()}: EUREGIO: {sname}").add_to(shapes_cluster)
    return fmap

