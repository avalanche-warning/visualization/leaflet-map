import awgeo
import awio
import configparser
import folium
import glob
import os
import rasterio
import sys

def _get_src_epsg(datapath: str) -> int:
    """Get EPSG code as set in the ini file / MeteoIO simulation."""
    sim_ini = os.path.join(datapath, "input/io.ini") 
    config = configparser.ConfigParser(strict=False) # allow MeteoIO's :: grouping
    config.read(sim_ini)
    config = awio.expanduser(config)

    try:
        epsg = config["INPUT"]["COORDPARAM"]
    except KeyError:
        print("[E] EPSG could not be determined from simulation ini file.")
        sys.exit()
    return epsg

def put_solar(fmap: folium.Map, domain: str, datapath: str) -> folium.Map:

    solfiles = glob.glob(os.path.join(datapath, "output", domain, "*.asc"))
    if not solfiles:
        print(f'[W] No solar data for domain "{domain}".')
        return fmap
    current_solar = solfiles[-1]

    src_epsg = _get_src_epsg(datapath) # read from the simulation's ini file
    dst_epsg_map = 3857 # Web Mercator
    dst_epsg_point = 4326 # WSG 84

    if (src_epsg != dst_epsg_map):
        path_reproj = f"{current_solar}.reprojected"
        if os.path.exists(path_reproj):
            print(f"[i] Would reproject, but trusting that existing file is EPSG:{dst_epsg_map}.")
        else:
            print(f"[i] Reprojecting from EPSG:{src_epsg} to EPSG:{dst_epsg_map}...")
            awgeo.reproject(src_epsg, dst_epsg_map, current_solar, path_reproj)
            current_solar = path_reproj

    print("[i] Opening raster...")
    with rasterio.open(current_solar, "r+") as solar:
        image = solar.read(1)
        solar.crs = rasterio.crs.CRS.from_epsg(dst_epsg_map) # set CRS of dataset
        latlon_bounds = awgeo.reproject_bounds(solar.crs, dst_epsg_point, solar.bounds) 
        
        _opacity = 0.8
        solar_raster = folium.raster_layers.ImageOverlay(
            image=image,
            origin="lower",
            bounds=latlon_bounds,
            opacity=_opacity,
            name=f"{domain.capitalize()}: Acuumulated SWR of last week [analysis]",
            show=False
        ).add_to(fmap)
        return fmap
