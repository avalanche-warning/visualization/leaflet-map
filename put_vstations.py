################################################################################
# Copyright 2023 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

import awset
import branca
import folium 
from folium.plugins import MarkerCluster
import os
import pandas as pd

def df_to_geojson(df, properties, lat='latitude', lon='longitude'):
    """
    Turn a dataframe containing point data into a geojson formatted python dictionary
    
    df : the dataframe to convert to geojson
    properties : a list of columns in the dataframe to turn into geojson feature properties
    lat : the name of the column in the dataframe that contains latitude data
    lon : the name of the column in the dataframe that contains longitude data
    """
    
    # create a new python dict to contain our geojson data, using geojson format
    geojson = {'type': 'FeatureCollection', 'features': []}

    # loop through each row in the dataframe and convert each row to geojson format
    for _, row in df.iterrows():
        feature = {
            "type": "Feature", # create a feature template to fill in
            "properties": {},
            "geometry": { "type":"Point", "coordinates": [] }
        }
        feature["geometry"]["coordinates"] = [row[lon],row[lat]]
        for prop in properties:
            feature["properties"][prop] = row[prop]
        # add this feature (aka, converted dataframe row) to the list of features inside our dict
        geojson["features"].append(feature)
    
    return geojson

def _make_description(feature):
    p_vstation = feature["properties"]["vstation"]
    p_elev = feature["properties"]["elev"]
    p_region_id = feature["properties"]["region_id"]
    description = f"Name: <b>{p_vstation}</b><br>Region: {p_region_id}<br>Elevation: {p_elev} m"
    return description

def _get_popup_frame():
    html = """
    <h1>Coming soon!</h1>
    """
    iframe = branca.element.IFrame(html=html, width=650, height=500)
    return iframe

def put_vstations(fmap: folium.Map, domain: str, vstations_file: str) -> folium.Map:
    """Add locations of virtual stations."""

    enabled = awset.is_enabled("forecasts", domain)
    if not enabled:
        return fmap
    if not os.path.exists(vstations_file):
        return fmap

    df_vstations = pd.read_csv(vstations_file)
    df_vstations = df_vstations[::5]
    fields = ["vstation", "elev", "region_id"]
    gdf = df_to_geojson(df_vstations, fields, lat="lat", lon="lon")
    marker_json = folium.GeoJson(
        gdf,
        name=f"{domain.capitalize()}: Virtual Stations",
        marker=folium.Circle(
            radius=180,
            color="black",
            fill_color="green",
            fill_opacity=0.4,
            weight=1
        ),
        show=False,
        tooltip=folium.GeoJsonTooltip(fields=fields),
        popup=folium.Popup(_get_popup_frame()),
        highlight_function=lambda x: {"fillOpacity": 0.8}
    )
    marker_json.add_to(fmap)

    return fmap
