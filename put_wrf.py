################################################################################
# Copyright 2023 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

import awset
from awwrf import wrf_plot
import folium
import glob
import os

def put_wrf(fmap: folium.Map, domain: str, wrf_output_path: str):
    """Visualize WRF output."""
    enabled = awset.is_enabled("wrf", domain)
    if not enabled:
        return fmap

    wrffiles = glob.glob(os.path.join(wrf_output_path, f"*_{domain}.nc"))
    if not wrffiles:
        print(f'[W] No WRF data for domain "{domain}"')
        return fmap
    wrffiles.sort()
    wrf_file = wrffiles[-1] # latest one

    # plot via awwrf:
    prefix = f"{domain.capitalize()}: "
    postfix = " [WRF forecast]"
    wrf_plot.plot_wind_pressure_interpol(wrf_file, fmap, name_prefix=prefix, name_postfix=postfix)
    wrf_plot.plot_snow_height(wrf_file, fmap, name_prefix=prefix, name_postfix=postfix)
    return fmap



